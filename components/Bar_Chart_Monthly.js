import { Bar } from 'react-chartjs-2'
import { useState, useEffect } from 'react'


export default function BarChartMonthly({ src }) {

    const [months, setMonths] = useState([])
    useEffect(() => {
        const monthsRef = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
        setMonths(monthsRef)
    }, [])

    const data = {
        labels: months,
        datasets: [
            {
                label: src.title,
                backgroundColor: 'red',
                borderColor: 'rgba(255,99,132,1)',
                borderWidth: 1,
                hoverBackgroundColor: 'rgba(255,99,132,0.4)',
                hoverBorderColor: 'rgba(255,99,132,1)',
                data: src.amount
            }
        ]
    }

    return (
        <>
            <Bar data={data} />
        </>
    )
}