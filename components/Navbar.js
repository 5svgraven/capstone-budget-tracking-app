import { useContext, useState } from 'react'
import { Navbar, Nav } from 'react-bootstrap'
import Link from 'next/link'
import UserContext from '../UserContext'

export default function NavBar() {
    const { user } = useContext(UserContext)
    const [expanded, setExpanded] = useState(false);
    return (
        <Navbar expand="lg" bg="dark" variant="dark" expanded={expanded}>
            <Navbar.Brand href="/" className="customColorNav">Budget Tracking</Navbar.Brand>
            {
                user.email
                    ?
                    <>
                        <Navbar.Toggle aria-controls="responsive-navbar-nav" onClick={() => setExpanded(expanded ? false : "expanded")} />
                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="mr-auto" onClick={() => setExpanded(false)}>
                                <Link href="/category" >
                                    <a className="nav-link customColorNav">Categories</a>
                                </Link>
                                <Link href="/records" >
                                    <a className="nav-link customColorNav">Records</a>
                                </Link>
                                <Link href="/monthly_income" >
                                    <a className="nav-link customColorNav">Monthly Income</a>
                                </Link>
                                <Link href="/monthly_expense" >
                                    <a className="nav-link customColorNav">Monthly Expense</a>
                                </Link>
                                <Link href="/trend" >
                                    <a className="nav-link customColorNav">Trend</a>
                                </Link>
                                <Link href="/breakdown" >
                                    <a className="nav-link customColorNav">Breakdown</a>
                                </Link>
                            </Nav>
                            <Nav>
                                <Link href="/logout">
                                    <a className="nav-link customColorNav">Logout</a>
                                </Link>
                            </Nav>

                        </Navbar.Collapse>
                    </> : null
            }

        </Navbar>
    )

}