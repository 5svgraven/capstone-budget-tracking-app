import { Jumbotron, Row, Col } from 'react-bootstrap'


export default function Banner({ data }) {
    const title = data.title
    const content = data.content
    return (
        <Row>
            <Col>
                <Jumbotron>
                    <h1>{title}</h1>
                    <p>{content}</p>
                </Jumbotron>
            </Col>
        </Row>
    )
}