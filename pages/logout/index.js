import { useContext, useEffect } from 'react'
import UserContext from '../../UserContext'
import Router from 'next/router'
import swal from 'sweetalert'

export default function Index() {
    const { unsetUser } = useContext(UserContext)

    useEffect(() => {
        unsetUser()
        swal('Logout Successfully', "Thank you for trusting us!", "success")
        Router.push('/')
    }, [])

    return null
}