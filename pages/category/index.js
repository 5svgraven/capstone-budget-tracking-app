import { useState, useEffect } from 'react'
import { Table, Button, Card, Dropdown, Modal, Form, Alert } from 'react-bootstrap'
import Router from 'next/router'
import swal from 'sweetalert'
import Head from 'next/head'


export default function Index() {

    const [addModalShow, setAddModalShow] = useState(false)
    const handleClose = () => {
        setCatName('')
        setCatType('Expense')
        setAddModalShow(false)
    }
    const [catName, setCatName] = useState('')
    const [catType, setCatType] = useState('Expense')
    const [incomeCategory, setIncomeCategory] = useState([])
    const [expenseCategory, setExpenseCategory] = useState([])

    function addNewIncomeCategory() {
        fetch(`https://budgettrackingapi.herokuapp.com/api/users/add-income-category`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                catName: catName,
            })
        })
            .then((res) => res.json())
            .then(data => {
                swal('Income name added', "You  have successfully added another income name", "success")
                Router.push('/category')
                setCatName('')
                setAddModalShow(false)
            })

    }

    function addNewExpenseCategory() {
        fetch(`https://budgettrackingapi.herokuapp.com/api/users/add-expense-category`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                catName: catName,
            })
        })
            .then((res) => res.json())
            .then(data => {
                swal('Expense name added', "You  have successfully added another expense name", "success")
                Router.push('/category')
                setCatName('')
                setAddModalShow(false)
            })

    }

    const categoryIncomeRow = incomeCategory.map(cat => {

        return (
            <>
                <tr key={cat._id}>
                    <td>{cat.catName}</td>
                    <td>Income</td>
                </tr>
            </>
        )
    })

    const categoryExpenseRow = expenseCategory.map(cat => {
        return (
            <>
                <tr key={cat._id}>
                    <td>{cat.catName}</td>
                    <td>Expense</td>
                </tr>
            </>
        )
    })

    useEffect(() => {
        fetch("https://budgettrackingapi.herokuapp.com/api/users/details", {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then((res) => res.json())
            .then(data => {
                setIncomeCategory(data.category.income)
                setExpenseCategory(data.category.expense)
            })

    }, [catName, setCatName])

    return (
        <>
            <Head>
                <title>Categories</title>
            </Head>
            <h1>Catgories</h1>
            <Button variant="success" className="mb-2" onClick={() => setAddModalShow(true)}>Add</Button>
            <Table striped hover bordered>
                <thead>
                    <tr>
                        <th>Category</th>
                        <th>Type</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        categoryIncomeRow
                    }
                    {
                        categoryExpenseRow
                    }
                </tbody>
            </Table>

            <Modal show={addModalShow} onHide={handleClose}
                centered
                size="lg"
                aria-labelledby="addCategoryId"
            >
                <Modal.Header closeButton>
                    <Modal.Title id="addCategoryId">
                        New Category
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group controlId="catNameId">
                            <Form.Label>Category Name</Form.Label>
                            <Form.Control type="text" placeholder="Enter Category Name" value={catName} onChange={(e) => setCatName(e.target.value)} required />
                        </Form.Group>
                        <Form.Group controlId="catTypeId">
                            <Form.Label>Category Type</Form.Label>
                            <Form.Control as="select" value={catType} onChange={(e) => setCatType(e.currentTarget.value)} required >
                                <option>Expense</option>
                                <option>Income</option>
                            </Form.Control>
                        </Form.Group>
                        {
                            catType === "Expense"
                                ? <Button variant="success" onClick={() => addNewExpenseCategory()}>Add New Category</Button>
                                : <Button variant="primary" onClick={() => addNewIncomeCategory()}>Add New Category</Button>

                        }

                    </Form>
                </Modal.Body>
            </Modal>

        </>
    )
}