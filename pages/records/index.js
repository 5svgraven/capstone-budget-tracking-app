import { useState, useEffect, Fragment } from 'react'
import { Card, Button, Form, Row, Col, Modal } from 'react-bootstrap'
import moment from 'moment'
import Router from 'next/router'
import swal from 'sweetalert'
import Head from 'next/head'

export default function Index() {

    const [searchRec, setSearchRec] = useState('')
    const [modalCatName, setModalCatName] = useState('Select')
    const [modalCatType, setModalCatType] = useState('Select')
    const [searchcatType, setSearchCatType] = useState('All')
    const [amount, setAmount] = useState(0)
    const [balance, setBalance] = useState(0)
    const [description, setDescription] = useState('')
    const [recordModalShow, setRecordModalShow] = useState(false)
    const [entries, setEntries] = useState([])
    const handleClose = () => {
        setModalCatType('Select')
        setRecordModalShow(false)
    }
    const [incomeCat, setIncomeCat] = useState([])
    const [expenseCat, setExpenseCat] = useState([])
    let newCatNameArrayIncome = []
    let newCatNameArrayExpense = []
    let showEntries = []
    let filteredEntries = []
    let catfilterentries = []


    useEffect(() => {
        fetch('https://budgettrackingapi.herokuapp.com/api/users/details', {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(res => res.json())
            .then(data => {
                if (data) {
                    setEntries(data.entries)
                    setBalance(data.balance)
                    setExpenseCat(data.category.expense)
                    setIncomeCat(data.category.income)
                }
            })
    }, [modalCatType, modalCatName])

    newCatNameArrayIncome = incomeCat.map(cat => <option>{cat.catName}</option>)
    newCatNameArrayExpense = expenseCat.map(cat => <option>{cat.catName}</option>)

    if (searchcatType === "All") {
        filteredEntries = entries.filter(entry => {
            if (searchRec === '') {
                return entries
            } else {
                return entry.description.toLowerCase().indexOf(searchRec.toLowerCase()) !== -1
            }
        })
    } else {
        filteredEntries = entries.filter(entry => {
            if (searchcatType === "Expense") {
                if (searchRec === '') {
                    return entry.categorytype.toLowerCase().indexOf(searchcatType.toLowerCase()) !== -1
                } else {
                    return entry.description.toLowerCase().indexOf(searchRec.toLowerCase()) !== -1 && entry.categorytype.toLowerCase().indexOf(searchcatType.toLowerCase()) !== -1
                }
            } else if (searchcatType === "Income") {
                if (searchRec === '') {
                    return entry.categorytype.toLowerCase().indexOf(searchcatType.toLowerCase()) !== -1
                } else {
                    return entry.description.toLowerCase().indexOf(searchRec.toLowerCase()) !== -1 && entry.categorytype.toLowerCase().indexOf(searchcatType.toLowerCase()) !== -1
                }
            } else {
                return entries
            }
        })
    }



    showEntries = filteredEntries.slice(0).reverse().map((entry) => {
        return (
            <>
                <Head>
                    <title>Records</title>
                </Head>
                {
                    entry.categorytype === "Income"
                        ?
                        <Card key={entry._id}>
                            <Card.Body>
                                <Row >
                                    <Col>
                                        <Card.Title>{entry.description}</Card.Title>
                                        <Card.Text> <span className="customIncome">{entry.categorytype}</span> ({entry.categoryname})</Card.Text>
                                        <p>{moment(entry.entryDate).format("LL")}</p>
                                    </Col>
                                    <Col className="text-right">
                                        <Card.Text className="customIncome">+ {entry.amount} </Card.Text>
                                        <Card.Text className="customIncome">{entry.currentBalance}</Card.Text>
                                    </Col>
                                </Row>

                            </Card.Body>
                        </Card>
                        :
                        <Card key={entry._id}>
                            <Card.Body>
                                <Row >
                                    <Col>
                                        <Card.Title>{entry.description}</Card.Title>
                                        <Card.Text>
                                            <span className="customExpense">{entry.categorytype}</span>({entry.categoryname})</Card.Text>
                                        <p>{moment(entry.entryDate).format("LL")}</p>
                                    </Col>
                                    <Col className="text-right">
                                        <Card.Text className="customExpense">- {entry.amount} </Card.Text>
                                        <Card.Text className="customExpense">{entry.currentBalance}</Card.Text>
                                    </Col>
                                </Row>

                            </Card.Body>
                        </Card>
                }

            </>

        )
    })

    function addNewRecord(e) {
        e.preventDefault()

        if (modalCatType === 'Income') {

            fetch('https://budgettrackingapi.herokuapp.com/api/users/add-entries', {
                method: "POST",
                headers: {
                    Authorization: `Bearer ${localStorage.getItem('token')}`,
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    balance: parseInt(amount) + parseInt(balance),
                    categorytype: modalCatType,
                    categoryname: modalCatName,
                    description: description,
                    currentBalance: parseInt(amount) + parseInt(balance),
                    amount: amount
                })
            })
                .then((res) => res.json())
                .then(data => {
                    swal('Added Successfully', 'You added another income to your account. Thank you', 'success')
                    setModalCatType('Select')
                    Router.push('/records')
                })
        } else {
            fetch('https://budgettrackingapi.herokuapp.com/api/users/add-entries', {
                method: "POST",
                headers: {
                    Authorization: `Bearer ${localStorage.getItem('token')}`,
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    balance: parseInt(balance) - parseInt(amount),
                    categorytype: modalCatType,
                    categoryname: modalCatName,
                    description: description,
                    currentBalance: parseInt(balance) - parseInt(amount),
                    amount: amount
                })
            })
                .then((res) => res.json())
                .then(data => {

                    swal('Added Successfully', 'You added another expenses to your account. Thank you and Please Control your expenses', 'success')
                    setModalCatType('Select')
                    Router.push('/records')
                })
        }

        setDescription("")
        setAmount(0)
        setRecordModalShow(false)

    }

    return (
        <>
            <h1>Records</h1>

            <Button className="mb-2" onClick={() => setRecordModalShow(true)} >Add New Record</Button>
            <Form>
                <Row>
                    <Col md={6} sm={12} className="customCol">
                        <Col>
                            <Form.Group controlId="searchRecId">
                                <Form.Control type="text" value={searchRec} placeholder="Search Record" onChange={(e) => setSearchRec(e.target.value)} />
                            </Form.Group>
                        </Col>
                    </Col>
                    <Col md={6} sm={12}>
                        <Form.Group controlId="optionId">
                            <Form.Control as="select" value={searchcatType} onChange={(e) => setSearchCatType(e.target.value)}>
                                <option>All</option>
                                <option>Expense</option>
                                <option>Income</option>
                            </Form.Control>
                        </Form.Group>
                    </Col>
                </Row>
            </Form>

            {showEntries}

            <Modal
                show={recordModalShow} onHide={handleClose}
                centered
                size="lg"
                aria-labelledby="addCategoryId"
            >
                <Modal.Header closeButton>
                    <Modal.Title id="addCategoryId">
                        New Record
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form onSubmit={e => addNewRecord(e)}>
                        <Form.Group controlId="catTypeId">
                            <Form.Label>Category Type</Form.Label>
                            <Form.Control as="select" onChange={(e) => setModalCatType(e.currentTarget.value)} required>
                                <option>Select</option>
                                <option>Expense</option>
                                <option>Income</option>
                            </Form.Control>
                            <Form.Group controlId="catNameId"  >
                                <Form.Label>Category Name</Form.Label>
                                {
                                    modalCatType === 'Select'
                                        ?
                                        <Form.Control readOnly placeholder="Select Category Type First" required />
                                        :
                                        <Form.Control as="select" onChange={(e) => setModalCatName(e.currentTarget.value)} required>
                                            {
                                                modalCatType === 'Expense'
                                                    ? newCatNameArrayExpense
                                                    : newCatNameArrayIncome
                                            }
                                        </Form.Control>

                                }
                            </Form.Group>
                            <Form.Group controlId="catNameId">
                                <Form.Label>Amount</Form.Label>
                                <Form.Control type="text" placeholder="Enter Amount" value={amount} onChange={(e) => setAmount(e.target.value)} required />
                            </Form.Group>
                            <Form.Group controlId="catNameId">
                                <Form.Label>Description</Form.Label>
                                <Form.Control type="text" placeholder="Enter Description" value={description} onChange={(e) => setDescription(e.target.value)} required />
                            </Form.Group>
                        </Form.Group>
                        <Button type="submit" variant="primary">Add New Record</Button>
                    </Form>
                </Modal.Body>
            </Modal>

        </>
    )
}