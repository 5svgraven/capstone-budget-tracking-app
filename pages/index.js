import Banner from '../components/Banner'
import { useContext } from 'react'
import Head from 'next/head'
import Login from '../pages/login/index'
import UserContext from '../UserContext'

export default function Index() {

  const { user } = useContext(UserContext)

  const data = {
    title: 'Budget Tracking App',
    content: 'To start, login using your email or by using your Google Account'
  }

  return (
    <>
      <Head>
        <title>Budget Tracker</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <Banner data={data} />
      {
        !user.email
          ? <Login />
          : null
      }
    </>
  )
}
