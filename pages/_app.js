import { useState, useEffect } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import '../styles/globals.css'
import { Container } from 'react-bootstrap'
import Navbar from '../components/Navbar'

import UserContext, { UserProvider } from '../UserContext'

function BudgetTracking({ Component, pageProps }) {

  const [user, setUser] = useState({
    email: null,
  })

  useEffect(() => {
    fetch(`https://budgettrackingapi.herokuapp.com/api/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then(res => res.json())
      .then(data => {
        if (data._id) {
          setUser({
            id: data._id,
            email: data.email
          })
        } else {
          setUser({
            id: null,
          })
        }
      })
  }, [setUser])

  const unsetUser = () => {
    localStorage.clear()
    setUser({
      email: null,
    })
  }



  return (
    <>
      <UserProvider value={{ user, setUser, unsetUser }}>
        <Navbar />
        <Container>
          <Component {...pageProps} />
        </Container>
      </UserProvider>
    </>
  )
}

export default BudgetTracking
