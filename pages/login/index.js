import { useState, useContext } from 'react'
import { Form, Button, Row, Col } from 'react-bootstrap'
import UserContext from '../../UserContext'
import Router from 'next/router'
import { GoogleLogin } from 'react-google-login'
import swal from 'sweetalert'

export default function Index() {
    //https://budgettrackingapi.herokuapp.com
    const { user, setUser } = useContext(UserContext)
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    function authenticate(e) {
        e.preventDefault()

        fetch('https://budgettrackingapi.herokuapp.com/api/users/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        }).then(res => {
            return res.json()
        }).then(data => {
            if (data.access) {
                localStorage.setItem('token', data.access)
                fetch('https://budgettrackingapi.herokuapp.com/api/users/details', {
                    headers: {
                        Authorization: `Bearer ${data.access}`
                    }
                }).then(res => {
                    return res.json()
                }).then(data => {
                    setUser({
                        id: data._id,
                        firstname: data.firstname,
                        lastname: data.lastname,
                        email: data.email,
                    })
                    swal("Login Successfully", `Welcome back ${data.firstname}`, 'success')
                    Router.push('/')
                })
            } else {
                swal("Login Failed", `Your email or password is incorrect`, 'error')
                setPassword('')
            }
        })
    }

    const authenticateGoogleToken = (response) => {
        const payload = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                tokenId: response.tokenId
            })
        }

        fetch('https://budgettrackingapi.herokuapp.com/api/users/verify-google-id-token', payload)
            .then(res => {
                return res.json()
            })
            .then(data => {
                if (typeof data.access !== 'undefined') {
                    localStorage.setItem('token', data.access)
                    fetch('https://budgettrackingapi.herokuapp.com/api/users/details', {
                        headers: {
                            Authorization: `Bearer ${data.access}`
                        }
                    }).then(res => {
                        return res.json()
                    }).then(data => {
                        setUser({
                            id: data._id,
                            firstname: data.firstname,
                            lastname: data.lastname,
                            email: data.email,
                        })
                        swal("Login Successfully", "You have login Successfully", "success");
                        Router.push('/')
                    })
                } else {
                    if (data.error === 'google-auth-error') {
                        swal("Google Auth Error", "Google Auth Failed", "error")
                    } else if (data.error === 'login-type-error') {
                        swal("Login Type Error", "You might have use different login procedure", "error")

                    }
                }
            })

    }



    return (
        <>
            <Row>
                <Col sm={12} md={6}>
                    <h1>Login</h1>
                    <Form onSubmit={(e) => authenticate(e)}>
                        <Form.Group controlId="userEmail">
                            <Form.Label>Email Address:</Form.Label>
                            <Form.Control type="email" placeholder="Enter Email" value={email} onChange={(e) => setEmail(e.target.value)} required />
                        </Form.Group>
                        <Form.Group controlId="password">
                            <Form.Label>Password:</Form.Label>
                            <Form.Control type="password" placeholder="Enter Password" value={password} onChange={(e) => setPassword(e.target.value)} />
                        </Form.Group>
                        <Button className="w-100 text-center d-flex justify-content-center" variant="primary" type="submit">Login</Button>
                    </Form>
                    <p className="mt-5">Don't have account? Register now</p>
                    <Button href="/register" variant="success">Register</Button>
                </Col>
                <Col sm={12} md={6} className="text-center">
                    <h1>Login using Google</h1>
                    <GoogleLogin
                        cookiePolicy={'single_host_origin'}
                        buttonText="Login using Google"
                        className="w-100 text-center my-3  d-flex justify-content-center"
                        clientId="665774562872-le4l421ps7mj4j18dth2610tunc3efso.apps.googleusercontent.com"
                        onSuccess={authenticateGoogleToken}
                        onFailure={authenticateGoogleToken}
                    />
                </Col>
            </Row>

        </>
    )
}