import { useEffect, useState } from 'react'
import BarChartMonthly from '../../components/Bar_Chart_Monthly'
import moment from 'moment'
import Head from 'next/head'

export default function Index() {

    const [entries, setEntries] = useState([])
    const [months, setMonths] = useState([])
    const [expense, setExpense] = useState([])

    useEffect(() => {
        fetch('https://budgettrackingapi.herokuapp.com/api/users/details', {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(res => res.json())
            .then(data => {
                setEntries(data.entries)
            })
    }, [])

    useEffect(() => {
        const monthsRef = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
        setMonths(monthsRef)
    }, [entries])

    useEffect(() => {
        setExpense(months.map(month => {
            let expense = 0;
            entries.forEach(entry => {
                if (moment(entry.entryDate).format('MMMM') === month) {

                    if (entry.categorytype === "Expense") {
                        expense += parseInt(entry.amount);
                    }

                }
            })
            return Math.round(expense)
        }))
    }, [months])


    const data = {
        title: "Monthly Expense",
        amount: expense
    }
    return (
        <>
            <Head>
                <title>Monthly Expense</title>
            </Head>
            <h1>Monthly Income</h1>
            <BarChartMonthly src={data} />
        </>
    )
}