import { useState, useEffect } from 'react'
import { Form, Button } from 'react-bootstrap'
import swal from 'sweetalert'
import Router from 'next/router'
import Head from 'next/head'

export default function Index() {

    const [firstname, setFirstname] = useState('')
    const [lastname, setLastname] = useState('')
    const [email, setEmail] = useState('')
    const [password1, setPassword1] = useState('')
    const [password2, setPassword2] = useState('')

    const [isActive, setIsActive] = useState(false)

    useEffect(() => {
        if ((firstname !== '' && lastname !== '' && email !== '' && password1 !== '' && password2 !== '') && password1 === password2) {

            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [firstname, lastname, email, password1, password2])

    function register(e) {
        e.preventDefault()
        fetch("https://budgettrackingapi.herokuapp.com/api/users/emailexist", {
            method: 'POST',
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email
            })
        })
            .then(res => res.json())
            .then(data => {
                if (data === false) {
                    fetch("https://budgettrackingapi.herokuapp.com/api/users", {
                        method: "POST",
                        headers: {
                            "Content-Type": "application/json"
                        },
                        body: JSON.stringify({
                            firstname: firstname,
                            lastname: lastname,
                            email: email,
                            password: password1
                        })
                    })
                        .then(res => res.json())
                        .then(data => {
                            if (data) {
                                swal("Registered Successfully", "Welcome to Zuitt Coding Bootcamp", "success")
                                Router.push('/login')
                            } else {
                                swal("Registration Failed", "Something went wrong", "error")
                            }

                        })
                } else {
                    swal("Registration Failed", "Email already exist", "error")
                }
            })
    }

    return (
        <>
            <Head>
                <title>Registration Page</title>
            </Head>
            <h1>Registration Page</h1>
            <Form onSubmit={(e) => register(e)}>
                <Form.Group controlId="firstnameId" >
                    <Form.Label>First Name</Form.Label>
                    <Form.Control type="text" value={firstname} onChange={(e) => setFirstname(e.target.value)} required />
                </Form.Group>
                <Form.Group controlId="lastnameId">
                    <Form.Label>Last Name</Form.Label>
                    <Form.Control type="text" value={lastname} onChange={(e) => setLastname(e.target.value)} required />
                </Form.Group>
                <Form.Group controlId="emailId">
                    <Form.Label>Email</Form.Label>
                    <Form.Control type="email" value={email} onChange={(e) => setEmail(e.target.value)} required />
                </Form.Group>
                <Form.Group controlId="pass1Id">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" value={password1} onChange={(e) => setPassword1(e.target.value)} required />
                </Form.Group>
                <Form.Group controlId="pass2Id">
                    <Form.Label>Confirm Password</Form.Label>
                    <Form.Control type="password" value={password2} onChange={(e) => setPassword2(e.target.value)} required />
                </Form.Group>
                {
                    isActive
                        ? <Button variant="primary" type="submit" id="submitBtn">Register</Button>
                        : <Button variant="secondary" type="submit" disabled>Register</Button>
                }
            </Form>
        </>
    )
}