import { Line } from 'react-chartjs-2'
import { Row, Col, Form } from 'react-bootstrap'
import { useState, useEffect } from 'react'
import moment from 'moment'
import Head from 'next/head'

export default function Index() {
    const [fromDate, setFromDate] = useState(new Date())
    const [toDate, setToDate] = useState(new Date())
    const [entries, setEntries] = useState([])
    const [labels, setLabels] = useState([])

    useEffect(() => {
        fetch('https://budgettrackingapi.herokuapp.com/api/users/details', {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(res => res.json())
            .then(data => {
                setEntries(data.entries)
            })
    }, [toDate])

    useEffect(() => {
        let tempdata = []
        let filteredEntries = entries.filter(entry => entry.categorytype === "Income");
        const entryAmount = filteredEntries.map(entry => {
            if (entry.categorytype === "Income") {
                return entry.amount

            } else {
                return 0
            }
        })

        setLabels(entryAmount)
    }, [toDate])

    const data = {
        labels: labels,
        datasets: [
            {
                label: 'My Income Trend',
                fill: false,
                lineTension: 0.1,
                backgroundColor: 'rgba(75,192,192,0.4)',
                borderColor: 'rgba(75,192,192,1)',
                borderCapStyle: 'butt',
                borderJoinStyle: 'miter',
                pointBorderColor: 'rgba(75,192,192,1)',
                pointBackgroundColor: 'blue',
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: 'rgba(75,192,192,1)',
                pointHoverBorderColor: 'blue',
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: labels
            }
        ]
    };

    return (

        <>
            <Head>
                <title>Trend</title>
            </Head>
            <h1> Balance Trend</h1>
            <Form>
                <Row>
                    <Col xs={12} md={6}>
                        <Form.Group>
                            <Form.Label>From</Form.Label>
                            <Form.Control type="date" value={fromDate} onChange={(e) => setFromDate(e.target.value)} required />
                        </Form.Group>
                    </Col>
                    <Col xs={12} md={6}>
                        <Form.Group>
                            <Form.Label>To</Form.Label>
                            <Form.Control type="date" value={toDate} onChange={(e) => setToDate(e.target.value)} required />
                        </Form.Group>
                    </Col>
                </Row>
            </Form>

            <Line data={data} />
        </>
    )
}