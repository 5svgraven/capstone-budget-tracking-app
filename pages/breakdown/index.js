import { Pie } from 'react-chartjs-2'
import { Row, Col, Form } from 'react-bootstrap'
import { useState, useEffect } from 'react'
import moment from 'moment'
import { colorRandomizer } from '../../helpers'
import Head from 'next/head'

export default function Index() {
    const [fromDate, setFromDate] = useState(new Date())
    const [toDate, setToDate] = useState(new Date())
    const [entries, setEntries] = useState([])
    const [expenses, setExpenses] = useState([])
    const [bgColor, setBgColor] = useState([])

    useEffect(() => {
        fetch('https://budgettrackingapi.herokuapp.com/api/users/details', {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(res => res.json())
            .then(data => {
                setEntries(data.entries)
            })
    }, [fromDate])

    useEffect(() => {
        let filterEntries = entries.filter(entry => entry.categorytype === "Expense")
        const entryExpense = filterEntries.map(entry => {
            if (moment(fromDate).format('MMMM Do YYYY') >= moment(entry.entryDate).format('MMMM Do YYYY')) {
                if (entry.categorytype === "Expense") {
                    return {
                        amount: entry.amount,
                        name: entry.categoryname,
                        color: `#${colorRandomizer()}`
                    }
                }
            }
        })
        setExpenses(entryExpense)

    }, [toDate])


    const label = expenses.map(expense => expense.name)
    const color = expenses.map(expense => expense.color)
    const amount = expenses.map(expense => expense.amount)

    const data = {
        labels: label,
        datasets: [{
            data: amount,
            backgroundColor: color,
        }]
    };

    return (

        <>
            <Head>
                <title>Breakdown</title>
            </Head>
            <h1> Breakdown</h1>
            <Form>
                <Row>
                    <Col xs={12} md={6}>
                        <Form.Group>
                            <Form.Label>From</Form.Label>
                            <Form.Control type="date" value={fromDate} onChange={(e) => setFromDate(e.target.value)} required />
                        </Form.Group>
                    </Col>
                    <Col xs={12} md={6}>
                        <Form.Group>
                            <Form.Label>To</Form.Label>
                            <Form.Control type="date" value={toDate} onChange={(e) => setToDate(e.target.value)} required />
                        </Form.Group>
                    </Col>
                </Row>
            </Form>

            <Pie data={data} />
        </>
    )
}